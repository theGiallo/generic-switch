test_switch_complete: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_complete -std=c++11 -D SWITCH_COMPLETE

test_switch_break: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_break -std=c++11 -D SWITCH_BREAK

test_switch_fallthrough: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_fallthrough -std=c++11 -D SWITCH_FALLTHROUGH

test_switch_nobrackets: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_nobrackets -std=c++11 -D SWITCH_NOBRACKETS

test_switch_break_nobrackets: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_break_nobrackets -std=c++11 -D SWITCH_BREAK -D SWITCH_NOBRACKETS

test_switch_nobrackets_fallthrough: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_nobrackets_fallthrough -std=c++11 -D SWITCH_NOBRACKETS -D SWITCH_FALLTHROUGH

test_switch_break_fallthrough: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_break_fallthrough -std=c++11 -D SWITCH_BREAK -D SWITCH_FALLTHROUGH

test_switch_break_nobrackets_fallthrough: switch.cpp generic_switch.h Makefile
	g++ switch.cpp -o test_switch_break_nobrackets_fallthrough -std=c++11 -D SWITCH_BREAK -D SWITCH_NOBRACKETS -D SWITCH_FALLTHROUGH

all: test_switch_complete test_switch_break test_switch_fallthrough test_switch_nobrackets test_switch_break_nobrackets test_switch_nobrackets_fallthrough test_switch_break_fallthrough test_switch_break_nobrackets_fallthrough Makefile

clean:
	rm test_switch_complete test_switch_break test_switch_fallthrough test_switch_nobrackets test_switch_break_nobrackets test_switch_nobrackets_fallthrough test_switch_break_fallthrough test_switch_break_nobrackets_fallthrough
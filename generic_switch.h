#ifndef SWITCH_H
#define SWITCH_H


// template <typename T, typename A, typename ...Args>
// bool equalsOR(const T& term, const A& arg1, const Args&... args)
// {
// 	return term==arg1 || equalsOR(term, args...);
// }

template <typename T, typename ...Args>
bool equalsOR(const T& term, const T& arg1, const Args&... args)
{
	return term==arg1 || equalsOR(term, args...);
}

// template <typename T, typename A>
// bool equalsOR(const T& term, const A& arg1)
// {
// 	return term==arg1;
// }

// // base
// template <typename T>
// bool equalsOR(const T& term)
// {
// 	return false;
// }

template <typename T>
bool equalsOR(const T& term, const T& arg1)
{
	return term==arg1;
}


#ifdef SWITCH_COMPLETE
	#ifndef SWITCH_BREAK
		#define SWITCH_BREAK
	#endif
	#ifndef SWITCH_NOBRACKETS
		#define SWITCH_NOBRACKETS
	#endif
	#ifndef SWITCH_FALLTHROUGH
		#define SWITCH_FALLTHROUGH
	#endif
#endif

// #if defined(SWITCH_FALLTHROUGH) && !defined(SWITCH_NOBRACKETS)
// 	#error You have to define SWITCH_NOBRACKETS in order to use fallthrough
// #endif

#ifdef SWITCH_BREAK

	#ifdef SWITCH_NOBRACKETS

		#ifdef SWITCH_FALLTHROUGH
			#define SWITCH(v) do{auto _SER_ = (v); bool _FALL_=false; {
			#define HCTIWS }}while (false);
			#define SWITCHEND HCTIWS
			#define CASE(...) }if (_FALL_ || equalsOR(_SER_,__VA_ARGS__)){_FALL_=true;
			#define DEFAULT }{
		#else
			#define SWITCH(v) do{auto _SER_ = (v); bool _DEFAULT_=true; {
			#define HCTIWS }}while(false);
			#define SWITCHEND HCTIWS
			#define CASE(...) }if (equalsOR(_SER_,__VA_ARGS__) && !(_DEFAULT_=false) ){
			#define DEFAULT }if (_DEFAULT_){
		#endif

	#else
		#ifdef SWITCH_FALLTHROUGH
			#define SWITCH(v) do{auto _SER_ = (v); bool _FALL_=false;
			#define HCTIWS }while (false);
			#define SWITCHEND HCTIWS
			#define CASE(...) if (_FALL_ || equalsOR(_SER_,__VA_ARGS__) && (_FALL_=true))
			#define DEFAULT 
		#else
			#define SWITCH(v) do{auto _SER_ = (v); bool _DEFAULT_=true;
			#define HCTIWS }while (false);
			#define SWITCHEND HCTIWS
			#define CASE(...) if (equalsOR(_SER_,__VA_ARGS__) && !(_DEFAULT_=false) )
			#define DEFAULT if (_DEFAULT_)
		#endif
	#endif

#else
	#ifdef SWITCH_NOBRACKETS
		#ifdef SWITCH_FALLTHROUGH
			#define SWITCH(v) {auto _SER_ = (v); bool _FALL_=false; {
			#define HCTIWS }}
			#define SWITCHEND HCTIWS
			#define CASE(...) }if (_FALL_ || equalsOR(_SER_,__VA_ARGS__)){_FALL_=true;
			#define DEFAULT }{
		#else
			#define SWITCH(v) {auto _SER_ = (v); bool _DEFAULT_=true; {
			#define HCTIWS }}
			#define SWITCHEND HCTIWS
			#define CASE(...) }if (equalsOR(_SER_,__VA_ARGS__) && !(_DEFAULT_=false)){
			#define DEFAULT }if (_DEFAULT_){
		#endif
	#else
		#ifdef SWITCH_FALLTHROUGH
			#define SWITCH(v) {auto _SER_ = (v); bool _FALL_=false; 
			#define HCTIWS }
			#define SWITCHEND HCTIWS
			#define CASE(...) if (_FALL_ || equalsOR(_SER_,__VA_ARGS__) && (_FALL_=true) )
			#define DEFAULT
		#else
			#define SWITCH(v) {auto _SER_ = (v); bool _DEFAULT_=true; 
			#define HCTIWS }
			#define SWITCHEND HCTIWS
			#define CASE(...) if (equalsOR(_SER_,__VA_ARGS__) && !(_DEFAULT_=false) )
			#define DEFAULT if (_DEFAULT_)
		#endif
	#endif
#endif

#endif // SWITCH_H